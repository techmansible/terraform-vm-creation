# Resource Group Details
#******************************************************

resource_location = "southeastasia"
resource_grp_name = "Terra"

# Compute Details
#******************************************************

compute_size = "Standard_DS1_v2"

# VM Details
#******************************************************

vm_name = "winvm1"
adm_usr = "vmadmin"
adm_pwd = "Silicon@1234567"

# Network Details
#******************************************************

vnet_name = "Terra-Vnet"
vnet_cidr = "11.0.0.0/22"
subnet_name = "default"
subnet_address = "11.0.0.0/27"

# OS Details - RHEL
#******************************************************

#publisher = "Redhat"
#offer = "RHEL"
#sku = "7-RAW"
#version = "7.2.2017090716"
publisher = "MicrosoftWindowsServer"
offer = "WindowsServer"
sku = "2016-Datacenter"
version = "latest"
