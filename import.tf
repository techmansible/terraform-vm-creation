# Import resource group
resource "azurerm_resource_group" "myterraformgroup" {
    name     = "${var.resource_grp_name}"
    location = "${var.resource_location}"

    tags {
        environment = "Terraform Demo"
    }
}

# Import virtual network
resource "azurerm_virtual_network" "myterraformnetwork" {
    name                = "${var.vnet_name}"
    address_space       = ["${var.vnet_cidr}"]
    location            = "${var.resource_location}"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"

    tags {
        environment = "Terraform Demo"
    }
}

# Import subnet
resource "azurerm_subnet" "myterraformsubnet" {
    name                 = "${var.subnet_name}"
    resource_group_name  = "${azurerm_resource_group.myterraformgroup.name}"
    virtual_network_name = "${azurerm_virtual_network.myterraformnetwork.name}"
    address_prefix       = "${var.subnet_address}"
}
