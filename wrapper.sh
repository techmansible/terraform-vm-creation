#!/bin/sh
terraform init
terraform import azurerm_resource_group.myterraformgroup /subscriptions/3ab8f508-6bc5-40ec-8ef5-af01563b8977/resourceGroups/Terra
terraform import azurerm_virtual_network.myterraformnetwork /subscriptions/3ab8f508-6bc5-40ec-8ef5-af01563b8977/resourceGroups/Terra/providers/Microsoft.Network/virtualNetworks/Terra-Vnet
terraform import azurerm_subnet.myterraformsubnet /subscriptions/3ab8f508-6bc5-40ec-8ef5-af01563b8977/resourceGroups/Terra/providers/Microsoft.Network/virtualNetworks/Terra-Vnet/subnets/default
terraform plan
terraform apply -auto-approve