# Configure the Microsoft Azure Provider
provider "azurerm" {
    subscription_id = "3ab8f508-6bc5-40ec-8ef5-af01563b8977"
    client_id       = "5b51ad34-a095-4627-ae1c-ec7b8e9532a6"
    client_secret   = "ee7bc8e0-aad5-4870-90a6-0e6d01a2af64"
    tenant_id       = "edf442f5-b994-4c86-a131-b42b03a16c95"
}

variable "resource_location" {
  type = "string"
}
variable "resource_grp_name" {
  type = "string"
}

variable "compute_size" {
  type = "string"
}

variable "vm_name" {
  type = "string"
}
variable "adm_usr" {
  type = "string"
}
variable "adm_pwd" {
  type = "string"
}

variable "vnet_name" {
  type = "string"
}
variable "vnet_cidr" {
  type = "string"
}
variable "subnet_name" {
  type = "string"
}
variable "subnet_address" {
  type = "string"
}

variable "publisher" {
  type = "string"
}
variable "offer" {
  type = "string"
}
variable "sku" {
  type = "string"
}
variable "version" {
  type = "string"
}

# Import resource group
data "azurerm_resource_group" "myterraformgroup" {
    name     = "${var.resource_grp_name}"
}

# Import virtual network
data "azurerm_virtual_network" "myterraformnetwork" {
    name                = "${var.vnet_name}"
    resource_group_name  = "${azurerm_resource_group.myterraformgroup.name}"
}

# Import subnet
data "azurerm_subnet" "myterraformsubnet" {
    name                 = "${var.subnet_name}"
    resource_group_name  = "${azurerm_resource_group.myterraformgroup.name}"
    virtual_network_name = "${azurerm_virtual_network.myterraformnetwork.name}"
}

# Create public IPs
resource "azurerm_public_ip" "myterraformpublicip" {
    name                         = "${var.vm_name}_PublicIP"
    location                     = "${var.resource_location}"
    resource_group_name          = "${azurerm_resource_group.myterraformgroup.name}"
    allocation_method            = "Dynamic"

    tags {
        environment = "Terraform Demo"
    }
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "myterraformnsg" {
    name                = "${var.vm_name}_nsg"
    location            = "${var.resource_location}"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
    tags {
        environment = "Terraform Demo"
    }
}
resource "azurerm_network_security_rule" "ssh_access" {
    name                       = "ssh-allow-rule"
    network_security_group_name       = "${azurerm_network_security_group.myterraformnsg.name}"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
    direction                  = "Inbound"
    access                     = "Allow"
    priority                   = 200
    source_address_prefix      = "*"
    source_port_range          = "*"
    destination_address_prefix = "*"
    destination_port_range     = "22"
    protocol                   = "TCP"
}
resource "azurerm_network_security_rule" "HTTP" {
    name                       = "http-allow-rule"
    network_security_group_name       = "${azurerm_network_security_group.myterraformnsg.name}"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
    direction                       = "Inbound"
    access                     = "Allow"
    priority                   = 210
    source_address_prefix      = "*"
    source_port_range          = "*"
    destination_address_prefix = "*"
    destination_port_range     = "80"
    protocol                   = "TCP"
}
resource "azurerm_network_security_rule" "HTTPS" {
    name                       = "https-allow-rule"
    network_security_group_name       = "${azurerm_network_security_group.myterraformnsg.name}"
    resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
    direction                       = "Inbound"
    access                     = "Allow"
    priority                   = 220
    source_address_prefix      = "*"
    source_port_range          = "*"
    destination_address_prefix = "*"
    destination_port_range     = "443"
    protocol                   = "TCP"
}

# Create network interface
resource "azurerm_network_interface" "myterraformnic" {
    name                      = "${var.vm_name}_NIC"
    location                  = "${var.resource_location}"
    resource_group_name       = "${azurerm_resource_group.myterraformgroup.name}"
    network_security_group_id = "${azurerm_network_security_group.myterraformnsg.id}"

    ip_configuration {
        name                          = "${var.vm_name}_NicConfiguration"
        subnet_id                     = "${azurerm_subnet.myterraformsubnet.id}"
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = "${azurerm_public_ip.myterraformpublicip.id}"
    }

    tags {
        environment = "Terraform Demo"
    }
}

# Generate random text for a unique storage account name
#resource "random_id" "randomId" {
#    keepers = {
#        # Generate a new ID only when a new resource group is defined
#        resource_group = "${azurerm_resource_group.myterraformgroup.name}"
#    }

#    byte_length = 8
#}

# Create storage account for boot diagnostics
#resource "azurerm_storage_account" "mystorageaccount" {
#    name                        = "diag${random_id.randomId.hex}"
#    resource_group_name         = "${azurerm_resource_group.myterraformgroup.name}"
#    location                    = "${var.resource_location}"
#    account_tier                = "Standard"
#    account_replication_type    = "LRS"

#    tags {
#        environment = "Terraform Demo"
#    }
#}

# Create data disk
#resource "azurerm_managed_disk" "myterraformdatadisk1" {
#  name                 = "${var.vm_name}_data1"
#  location             = "${var.resource_location}"
#  resource_group_name  = "${azurerm_resource_group.myterraformgroup.name}"
#  storage_account_type = "Standard_LRS"
#  create_option        = "Empty"
#  disk_size_gb         = "10"

#  tags = {
#    environment = "Terraform Demo"
#  }
#}

# Create virtual machine
resource "azurerm_virtual_machine" "myterraformvm" {
    name                  = "${var.vm_name}"
    location              = "${var.resource_location}"
    resource_group_name   = "${azurerm_resource_group.myterraformgroup.name}"
    network_interface_ids = ["${azurerm_network_interface.myterraformnic.id}"]
    vm_size               = "${var.compute_size}"

    storage_os_disk {
        name              = "${var.vm_name}_osDisk"
        caching           = "ReadWrite"
        create_option     = "FromImage"
        managed_disk_type = "Premium_LRS"
    }

    storage_data_disk {
        name              = "${var.vm_name}_datadisk1"
        caching           = "ReadWrite"
        create_option     = "Empty"
        disk_size_gb      = "10"
        lun               = "1"
        managed_disk_type = "Standard_LRS"
    }

    storage_image_reference {
        publisher = "${var.publisher}"
        offer     = "${var.offer}"
        sku       = "${var.sku}"
        version   = "${var.version}"
    }

    os_profile {
        computer_name  = "${var.vm_name}"
        admin_username = "${var.adm_usr}"
        admin_password = "${var.adm_pwd}"
    }

    os_profile_windows_config {
        enable_automatic_upgrades = false
    }
#    os_profile_linux_config {
#        disable_password_authentication = false
#    }

#    boot_diagnostics {
#        enabled = "false"
#        #storage_uri = "${azurerm_storage_account.mystorageaccount.primary_blob_endpoint}"
#    }

    tags {
        environment = "Terraform Demo"
    }
}
